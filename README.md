# N-Gramator

## The project
N-gramator is a n-gram generator website from a string of text. N-grams are a contiguous sequence of n words from a string of text, and have many applications from full-text search indexes to machine learning.

**To see the project running, go to: [https://n-grams.vercel.app/](https://n-grams.vercel.app/)**

## The skeleton
The tooling and criteria used for this project was:
* Create React App
* Jest testing
* ESLint

## How to run the app?
`yarn start` runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 

## How it works?
Well, when you go into this website, you will find a input field. You can write a string there, then click the 'Get N-Grams!" button and the site will display all the n-grams generated with your string.<br>

## Next steps
* **Responsive layout:** add full responsive layout.