import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import GetNGrams, { searchNGrams } from './GetNGrams';
import '@testing-library/jest-dom';

describe('GetNGrams', () => {
  describe('Test Algorithm', () => {
    it("should get N-Grams when there's a string", () => {
      const ngrams = searchNGrams('Show me the code');
      expect(ngrams).toStrictEqual(
        ['Show',
          'Show me',
          'Show me the',
          'Show me the code',
          'me',
          'me the',
          'me the code',
          'the',
          'the code',
          'code'],
      );
    });

    it('should get N-Grams ignoring puntuation', () => {
      const ngrams = searchNGrams('Show me the code.');
      expect(ngrams).toStrictEqual(
        ['Show',
          'Show me',
          'Show me the',
          'Show me the code',
          'me',
          'me the',
          'me the code',
          'the',
          'the code',
          'code'],
      );
    });

    it('should an empty array if there is no string term', () => {
      const ngrams = searchNGrams('');
      expect(ngrams).toStrictEqual([]);
    });
  });

  describe('Test flow', () => {
    it('should show the ngrams after click the button', () => {
      render(<GetNGrams />);
      const input = screen.queryByTestId('ngram-input');
      fireEvent.change(input, { target: { value: 'Show me the code' } });
      const button = screen.queryByTestId('ngram-btn');
      fireEvent.click(button);
      const results = screen.queryByTestId('ngram-multiple');
      expect(results.children.length).toBe(10);
    });

    it('should show an empty state if there is NO string term', () => {
      render(<GetNGrams />);
      const input = screen.queryByTestId('ngram-input');
      fireEvent.change(input, { target: { value: '' } });
      const button = screen.queryByTestId('ngram-btn');
      fireEvent.click(button);
      const emptyState = screen.getByTestId('ngram-none');
      expect(emptyState).toBeVisible();
    });

    it('should NOT show the empty state if there is string term', () => {
      render(<GetNGrams />);
      const input = screen.queryByTestId('ngram-input');
      fireEvent.change(input, { target: { value: 'Show me the code' } });
      const button = screen.queryByTestId('ngram-btn');
      fireEvent.click(button);
      const emptyState = screen.queryByTestId('ngram-none');
      expect(emptyState).toBeNull();
    });

    it('should NOT show an empty list if there is NO string term', () => {
      render(<GetNGrams />);
      const input = screen.queryByTestId('ngram-input');
      fireEvent.change(input, { target: { value: '' } });
      const button = screen.queryByTestId('ngram-btn');
      fireEvent.click(button);
      const results = screen.queryByTestId('ngram-multiple');
      expect(results).toBeNull();
    });
  });
});
