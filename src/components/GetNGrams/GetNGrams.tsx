import React, { useState } from 'react';
import './GetNGrams.scss';

const cleanTerm = (stringTerm:string) => stringTerm.replace(/[^\w\s]|_/g, '').replace(/\s+/g, ' ');

const getList = (terms:string[]) => {
  const ngramsList:string[] = [];
  let prevTerm = '';
  for (let i = 0; i < terms.length; i++) {
    if (i === 0) {
      ngramsList.push(terms[i]);
      prevTerm = terms[i];
    } else {
      prevTerm = `${prevTerm} ${terms[i]}`;
      ngramsList.push(prevTerm);
    }
  }
  return ngramsList;
};

export const searchNGrams = (stringTerm:string) => {
  let ngrams:string[] = [];
  stringTerm = cleanTerm(stringTerm);
  if (stringTerm !== '') {
    const terms:string[] = stringTerm.split(' ');
    let ngramsList:string[] = [];
    while (terms.length) {
      ngramsList = [...ngramsList, ...getList(terms)];
      terms.shift();
    }
    ngrams = [...ngramsList];
  }
  return ngrams;
};

const NGramList = ({ ngrams }: { ngrams:string[]}) => (
  <ul className="getngram-list" data-testid="ngram-multiple">
    {ngrams.map((item) => <li className="getngram-list-item" key={item}>{item}</li>)}
  </ul>
);

const MultipleResult = ({ stringTerm, ngrams }: { stringTerm:string, ngrams:string[]}) => (
  <div>
    <h3>
      With your     
      {' '}
      {stringTerm.split(' ').length}
      {' '}
      word text, you create     
      {' '}
      {ngrams.length}
      {' '}
      n-grams!
    </h3>
    <NGramList ngrams={ngrams} />
  </div>
);

const Results = ({ stringTerm, ngrams }: { stringTerm:string, ngrams:string[]}) => (ngrams.length
  ? <MultipleResult stringTerm={stringTerm} ngrams={ngrams} />
  : <span className="getngram-empty" data-testid="ngram-none">No N-grams</span>);



const GetNGrams = () => {
  const [stringTerm, setStringTerm] = useState('');
  const [ngrams, setNgrams] = useState<string[]>([]);
  return (
    <div className="getngram">
      <input className="getngram-input" type="text" placeholder="Insert a string" data-testid="ngram-input" onChange={(e) => setStringTerm(e.target.value)} />
      <button className="getngram-button" type="button" onClick={() => setNgrams(searchNGrams(stringTerm))} data-testid="ngram-btn">
        Get N-Grams!
      </button>
      <Results stringTerm={stringTerm} ngrams={ngrams} />
    </div>
  );
};

export default GetNGrams;
