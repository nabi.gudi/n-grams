import React from 'react';
import GetNGrams from '../../components/GetNGrams/GetNGrams';
import './Home.scss';

function Home() {
  return (
    <div className="home-container">
      <h1 className="home-title title">N-Gramator</h1>
      <section className="home-section">
        <h2 className="home-section-title title">What is a N-gram?</h2>
        <p className="home-section-text text">
          N-grams are a contiguous sequence of n words from a string of text, and have many applications from
          full-text search indexes to machine learning. You can find more information about N-Grams in
          this{' '}
          <a className="link" href="https://en.wikipedia.org/wiki/N-gram">link</a>
          .
        </p>
      </section>
      <main className="home-ngrams">
        <h2 className="home-ngrams-title title">Try it out!</h2>
        <p className="home-ngrams-description text">
          Write in the box below a sentence, it can be as long as you want, then click on the button{' '}
          <strong><em>Get N-grams!</em></strong>
          {' '}
          and discover how many n-grams can come from your sentence.
        </p>
        <GetNGrams />
      </main>
      <footer className="footer title">
        Nabila Gudiño Ochoa - 2021 - N-Grams Code Challenge
      </footer>
    </div>
  );
}

export default Home;
