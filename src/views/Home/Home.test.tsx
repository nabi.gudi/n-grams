import { render, screen } from '@testing-library/react';
import Home from './Home';

describe('Home', () => {
  test('The Home view renders properly', () => {
    render(<Home />);
    expect(screen.getByText(/N-Gramator/i)).toBeInTheDocument();
  });
});
