import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {
  test('The App renders properly', () => {
    render(<App />);
    expect(screen.getByText(/N-Gramator/i)).toBeInTheDocument();
  });
});
